import java.util.Random;

public class Autobot {

    private static Autobot instance = null;
    private String name;

    private Autobot(String name) {
        this.name = name;
    }

    public static Autobot getInstance() {
        if (instance == null) {
            Random rand = new Random();
            String[] names = {"Optimus Prime", "Bumblebee", "Ironhide", "Ratchet", "Jazz", "Cliffjumper"};
            String randomName = names[rand.nextInt(names.length)];
            instance = new Autobot(randomName);
        }
        return instance;
    }

    public String getName() {
        return name;
    }
}
