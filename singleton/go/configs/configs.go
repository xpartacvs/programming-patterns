package configs

import (
	"os"
	"sync"
)

type Db struct {
	user string
	pass string
}

var (
	db     *Db
	dbOnce sync.Once
)

func DB() *Db {
	dbOnce.Do(func() {
		db = &Db{
			user: os.Getenv("DB_USER"),
			pass: os.Getenv("DB_PASS"),
		}
	})
	return db
}

func (db *Db) User() string {
	return db.user
}

func (db *Db) Pass() string {
	return db.pass
}
