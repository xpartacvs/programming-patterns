package main

import (
	"fmt"
	"singleton/configs"
)

func main() {
	fmt.Printf("DB User: %s\n", configs.DB().User())
	fmt.Printf("DB Pass: %s\n", configs.DB().Pass())
}
