package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"reflect"
	"runtime"
	"strconv"
	"sync"
)

type MemInfo struct {
	Alloc      string `json:"     alloc"`
	TotalAlloc string `json:"totalAlloc"`
	Mallocs    string `json:"   mallocs"`
	Frees      string `json:"     frees"`
}

type ResponseTemplate struct {
	Error   bool    `json:"error"`
	Message string  `json:"message"`
	Memory  MemInfo `json:"memory"`
}

var csvPool sync.Pool = sync.Pool{
	New: func() any {
		log.Default().Println("New csvPool")
		fHandle, err := os.Open("data.csv")
		if err != nil {
			return err
		}
		defer fHandle.Close()

		csvReader := csv.NewReader(fHandle)
		records, err := csvReader.ReadAll()
		if err != nil {
			return err
		}

		tokens := make(map[string]string)
		for _, record := range records {
			tokens[record[0]] = record[1]
		}

		return tokens
	},
}

var logger *log.Logger

func main() {
	logger = log.New(os.Stdout, "", log.LstdFlags)

	http.HandleFunc("/user", handleHash)

	if err := http.ListenAndServe(":8081", nil); err != nil {
		logger.Fatalln(err.Error())
	}
}

func handleHash(w http.ResponseWriter, r *http.Request) {
	res := NewResponse()

	strId := r.URL.Query().Get("id")
	if _, err := strconv.Atoi(strId); err != nil {
		res.Error = true
		res.Message = "Invalid ID"
		if errSend := SendJSON(w, http.StatusBadRequest, res); errSend != nil {
			logger.Println(errSend.Error())
		}
		return
	}

	obj := csvPool.Get()
	res.SetMemoryInfo(nil)
	if reflect.TypeOf(obj).String() == "error" {
		objError := obj.(error)
		res.Error = true
		res.Message = objError.Error()
		if errSend := SendJSON(w, http.StatusInternalServerError, res); errSend != nil {
			logger.Println(errSend.Error())
		}
		logger.Println(objError.Error())
		return
	}

	hases := obj.(map[string]string)
	var statusCode int
	if hash, ok := hases[strId]; ok {
		res.Message = hash
		statusCode = http.StatusOK
		csvPool.Put(hases)
	} else {
		res.Message = "Not found"
		statusCode = http.StatusNotFound
	}

	if errSend := SendJSON(w, statusCode, res); errSend != nil {
		logger.Println(errSend.Error())
	}
}

func SendJSON(w http.ResponseWriter, statusCode int, data *ResponseTemplate) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	bytesData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	_, err = w.Write(bytesData)
	if err != nil {
		return err
	}

	return nil
}

func NewResponse() *ResponseTemplate {
	r := &ResponseTemplate{}
	r.SetMemoryInfo(nil)
	return r
}

func (r *ResponseTemplate) SetMemoryInfo(mi *runtime.MemStats) {
	var m *runtime.MemStats
	if mi == nil {
		m = new(runtime.MemStats)
		runtime.ReadMemStats(m)
	} else {
		m = mi
	}

	alloc := fmt.Sprintf("%12d", m.Alloc)
	totalAlloc := fmt.Sprintf("%12d", m.TotalAlloc)
	mallocs := fmt.Sprintf("%12d", m.Mallocs)
	frees := fmt.Sprintf("%12d", m.Frees)

	r.Memory.Alloc = alloc + " bytes"
	r.Memory.TotalAlloc = totalAlloc + " bytes"
	r.Memory.Mallocs = mallocs + " times"
	r.Memory.Frees = frees + " times"
}
